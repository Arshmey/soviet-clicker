package com.ancme.soviet.win;

import java.awt.Toolkit;

import javax.swing.JFrame;

import com.ancme.soviet.loader.ChangeTranslate;
import com.ancme.soviet.loader.MainLoader;
import com.ancme.soviet.logic.AutoClicker;
import com.ancme.soviet.logic.Balance;
import com.ancme.soviet.logic.DifficultyChanged;
import com.ancme.soviet.logic.Job;
import com.ancme.soviet.logic.Reset;
import com.ancme.soviet.logic.Upgrade;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class Difficulty {

	private JFrame frame;
	public JLabel BACKGROUND;
	private int i;
	public Soviet s;
	private JLabel Author;

	public Difficulty(MainLoader main, Upgrade up, Balance mon, Job job, ChangeTranslate CT, AutoClicker AC) {
		initialize(main, up, mon, job, CT, AC);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(MainLoader main, Upgrade up, Balance mon, Job job, ChangeTranslate CT, AutoClicker AC) {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(main.IMG[1]));
		frame.setTitle(main.TRANSLATE[1]);
		frame.setBounds(300, 250, 506, 309);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		
		Author = new JLabel("Ancora Studion 2019-2021. Ver. 2.1");
		Author.setForeground(Color.WHITE);
		Author.setFont(new Font("Tahoma", Font.BOLD, 12));
		Author.setBounds(0, 0, 256, 14);
		frame.getContentPane().add(Author);
		
		JButton EASY = new JButton(main.TRANSLATE[5]);
		EASY.setToolTipText("Easy");
		EASY.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				i = 2;
				new DifficultyChanged(main, up, mon, i, job, AC);
				frame.setVisible(false);
			}
		});
		EASY.setBounds(57, 84, 123, 23);
		frame.getContentPane().add(EASY);
		
		JButton NORMAL = new JButton(main.TRANSLATE[6]);
		NORMAL.setToolTipText("Normal");
		NORMAL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				i = 3;
				new DifficultyChanged(main, up, mon, i, job, AC);
				frame.setVisible(false);
			}
		});
		NORMAL.setBounds(179, 84, 123, 23);
		frame.getContentPane().add(NORMAL);
		
		JButton HARD = new JButton(main.TRANSLATE[7]);
		HARD.setToolTipText("Hard(Standart)");
		HARD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				i = 4;
				new DifficultyChanged(main, up, mon, i, job, AC);
				frame.setVisible(false);
			}
		});
		HARD.setBounds(301, 84, 123, 23);
		frame.getContentPane().add(HARD);
		
		JButton SETLANG = new JButton(main.TRANSLATE[24]);
		SETLANG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Reset(CT, main);
			}
		});
		SETLANG.setBounds(401, 0, 89, 23);
		frame.getContentPane().add(SETLANG);
		
		BACKGROUND = new JLabel("x");
		BACKGROUND.setBounds(0, 0, 500, 280);
		frame.getContentPane().add(BACKGROUND);
						
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}
}
