package com.ancme.soviet.win;

import javax.swing.JFrame;

import com.ancme.soviet.logic.Balance;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class Debug {

	private JFrame frame;

	public Debug(Balance mon) {
		initialize(mon);
	}

	private void initialize(Balance mon) {
		frame = new JFrame();
		frame.setTitle("Hey, it's not fair");
		frame.setBounds(100, 100, 186, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Money");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mon.setMoney(mon.getMoney() + 500000000l);
			}
		});
		btnNewButton.setBounds(32, 25, 100, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Ancora Studion 2019-2021.");
		lblNewLabel.setBounds(0, 0, 170, 14);
		frame.getContentPane().add(lblNewLabel);
		frame.setVisible(true);
	}
}
