package com.ancme.soviet.win;

import java.io.IOException;

import javax.swing.ImageIcon;

import com.ancme.soviet.loader.ChangeTranslate;
import com.ancme.soviet.loader.MainLoader;
import com.ancme.soviet.logic.AutoClicker;
import com.ancme.soviet.logic.Balance;
import com.ancme.soviet.logic.Job;
import com.ancme.soviet.logic.Upgrade;

public class Starter {
		
	public static void main(String[] args) throws IOException {
	
		ChangeTranslate CT = new ChangeTranslate(); //Setup Translate
		MainLoader main = new MainLoader(CT); //Loader Files
		Upgrade up = new Upgrade(); //Is upgrade
		Balance mon = new Balance(); //Is balance
		Job job = new Job(); //Is Job
		AutoClicker AC = new AutoClicker();//AutoClicker
		Difficulty dif = new Difficulty(main, up, mon, job, CT, AC); //Difficulty
		dif.BACKGROUND.setIcon(new ImageIcon(main.IMG[2])); //Setup Background "Difficulty"
	
	}
}
