package com.ancme.soviet.win;

import javax.swing.JFrame;

import com.ancme.soviet.loader.MainLoader;
import com.ancme.soviet.logic.AutoClicker;
import com.ancme.soviet.logic.Balance;
import com.ancme.soviet.logic.Event;
import com.ancme.soviet.logic.GroundMusic;
import com.ancme.soviet.logic.Job;
import com.ancme.soviet.logic.Upgrade;

import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;

public class Soviet {

	private JFrame frmSovietClikerCapitalist;
	private JLabel Author;
	public JLabel BACKGROUND;
	public JLabel Balance;
	private JButton SELL;
	public JLabel COST;
	public JLabel LVL;
	private JLabel INFO;
	public JButton REST;
	public JButton UP;
	private JButton Mute;
	public JLabel YOURE_JOB;
	public JButton AUTO;
	
	public Soviet(MainLoader main, Upgrade up, Balance mon, int i, GroundMusic gM, Job job, AutoClicker AC) {
		initialize(main, up, mon, i, gM, job, AC);
	}

	private void initialize(MainLoader main, Upgrade up, Balance mon, int i, GroundMusic gM, Job job, AutoClicker AC) {
		frmSovietClikerCapitalist = new JFrame();
		frmSovietClikerCapitalist.setResizable(false);
		frmSovietClikerCapitalist.setIconImage(Toolkit.getDefaultToolkit().getImage(main.IMG[1]));
		frmSovietClikerCapitalist.setTitle(main.TRANSLATE[i]);
		frmSovietClikerCapitalist.setBounds(300, 250, 506, 309);
		frmSovietClikerCapitalist.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSovietClikerCapitalist.setVisible(true);
		frmSovietClikerCapitalist.getContentPane().setLayout(null);
		
		Author = new JLabel("Ancora Studion 2019-2021. Ver. 2.1");
		Author.setForeground(Color.WHITE);
		Author.setFont(new Font("Tahoma", Font.BOLD, 12));
		Author.setBounds(0, 0, 244, 14);
		frmSovietClikerCapitalist.getContentPane().add(Author);
		
		Balance = new JLabel(main.TRANSLATE[8]+": "+mon.getMoney()+" "+main.TRANSLATE[16]);
		Balance.setForeground(Color.WHITE);
		Balance.setFont(new Font("Tahoma", Font.BOLD, 12));
		Balance.setBounds(10, 59, 480, 14);
		frmSovietClikerCapitalist.getContentPane().add(Balance);
		
		YOURE_JOB = new JLabel(main.TRANSLATE[18]+": "+main.TRANSLATE[19+job.getN()]+". "+main.TRANSLATE[17]+
				": "+job.getStandart()[job.getN()]+" "+main.TRANSLATE[16]);
		YOURE_JOB.setForeground(Color.WHITE);
		YOURE_JOB.setFont(new Font("Tahoma", Font.BOLD, 12));
		YOURE_JOB.setBounds(84, 76, 339, 14);
		frmSovietClikerCapitalist.getContentPane().add(YOURE_JOB);
		
		SELL = new JButton(main.TRANSLATE[9]);
		SELL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				job.Sell(up, mon);
			}
		});
		SELL.setBounds(184, 90, 128, 23);
		frmSovietClikerCapitalist.getContentPane().add(SELL);
		
		LVL = new JLabel(main.TRANSLATE[10]+": "+up.getLVL());
		LVL.setForeground(Color.WHITE);
		LVL.setFont(new Font("Tahoma", Font.BOLD, 12));
		LVL.setBounds(248, 133, 242, 14);
		frmSovietClikerCapitalist.getContentPane().add(LVL);
		
		COST = new JLabel(main.TRANSLATE[11]+": "+up.getCost()+" "+main.TRANSLATE[16]);
		COST.setForeground(Color.WHITE);
		COST.setFont(new Font("Tahoma", Font.BOLD, 12));
		COST.setBounds(10, 133, 234, 14);
		frmSovietClikerCapitalist.getContentPane().add(COST);
			
		UP = new JButton(main.TRANSLATE[12]);
		UP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				up.EventUpgrade(mon, job);
			}
		});
		UP.setBounds(184, 158, 128, 23);
		frmSovietClikerCapitalist.getContentPane().add(UP);
		
		INFO = new JLabel(main.TRANSLATE[13]);
		INFO.setForeground(Color.WHITE);
		INFO.setFont(new Font("Tahoma", Font.BOLD, 15));
		INFO.setBounds(26, 211, 452, 23);
		frmSovietClikerCapitalist.getContentPane().add(INFO);
		
		REST = new JButton(main.TRANSLATE[14]);
		REST.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Event(main, mon);
			}
		});
		REST.setBounds(165, 235, 174, 23);
		frmSovietClikerCapitalist.getContentPane().add(REST);
		
		Mute = new JButton("");
		Mute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gM.MuteOrUnmute();
			}
		});
		Mute.setIcon(new ImageIcon(main.IMG[3]));
		Mute.setBounds(458, 0, 32, 32);
		frmSovietClikerCapitalist.getContentPane().add(Mute);
		
		AUTO = new JButton(main.TRANSLATE[25]);
		AUTO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					AC.StartClick(job, up, mon);
			}
		});
		AUTO.setBounds(379, 235, 100, 23);
		AUTO.setVisible(false);
		frmSovietClikerCapitalist.getContentPane().add(AUTO);
		
		BACKGROUND = new JLabel("x");
		BACKGROUND.setIcon(new ImageIcon(main.IMG[2]));
		BACKGROUND.setBounds(0, 0, 500, 280);
		frmSovietClikerCapitalist.getContentPane().add(BACKGROUND);
									
	}
}
