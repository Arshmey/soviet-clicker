package com.ancme.soviet.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ChangeTranslate {

	private int Choosed = 1;

	public ChangeTranslate() throws IOException {
		LoadLoc();
	}
	
	public int getChoosed() {
		return Choosed;
	}

	public void setChoosed(int choosed) {
		Choosed = choosed;
	}
	
	public void Change(MainLoader ML) {
		if(Choosed != ML.LOCALIZATION.length-1) {
			Choosed = Choosed + 1;
			System.out.println(Choosed);
		}else {
			Choosed = 1;
			System.out.println(Choosed);
		}
	}
	
	public void SaveLoc() throws IOException {
		FileOutputStream fos = new FileOutputStream(new File("Sovet File\\Translate\\Setup.ancme"));
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeInt(Choosed);
        out.close();
	}
	
	public void LoadLoc() throws IOException {
		FileInputStream fost = new FileInputStream(new File("Sovet File\\Translate\\Setup.ancme"));
		ObjectInputStream in = new ObjectInputStream(fost);
		Choosed = in.readInt();
        in.close();
	}

}
