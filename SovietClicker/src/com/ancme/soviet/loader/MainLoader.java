package com.ancme.soviet.loader;

import java.io.File;
import java.util.Scanner;

public class MainLoader {
	
	private String LOCALIZATION_SET = "";
	public String LOCALIZATION[];
	private String TRANSLATE_SET = "";
	public String TRANSLATE[];
	private String IMG_SET = "";
	public String IMG[];
	
	public MainLoader(ChangeTranslate CT) {
		Localization();
		Translate(CT);
		IMG();
	}
	
	private void Localization() {
		try {		
			Scanner loaderloc = new Scanner(new File("Sovet File\\Localization.ancme")); 
			while(loaderloc.hasNext())
			LOCALIZATION_SET += loaderloc.nextLine();
			LOCALIZATION = LOCALIZATION_SET.split("list: ");
			System.out.println(LOCALIZATION_SET);
			loaderloc.close();
		 } catch (Exception e) { }
	}
	
	private void Translate(ChangeTranslate CT) {
		try {		
			Scanner translate = new Scanner(new File(LOCALIZATION[CT.getChoosed()])); 
			while(translate.hasNext())
			TRANSLATE_SET += translate.nextLine();
			TRANSLATE = TRANSLATE_SET.split("lang: ");
			System.out.println(TRANSLATE_SET);
			translate.close();
		 } catch (Exception e) { }
	}
	
	private void IMG() {
		try {		
			Scanner loaderimg= new Scanner(new File("Sovet File\\IMG.ancme")); 
			while(loaderimg.hasNext())
			IMG_SET += loaderimg.nextLine();
			IMG = IMG_SET.split("img: ");
			System.out.println(IMG_SET);
			loaderimg.close();
		 } catch (Exception e) { }
	}
}
