package com.ancme.soviet.logic;

import javax.swing.Timer;

public class MusicUpd {

	Timer UPD;
	
	public MusicUpd(GroundMusic GM, int i) {
		Music(GM, i);
	}
	
	private void Music(GroundMusic GM, int i) {
		UPD = new Timer(0, e -> {
			if(GM.GM.isRunning() == false) { GM.RESTART(); }
		});
		UPD.start();
	}
	
}
