package com.ancme.soviet.logic;

import javax.swing.Timer;

import com.ancme.soviet.loader.MainLoader;
import com.ancme.soviet.win.Soviet;

public class TimeUpdate {

	Timer UPD;
	boolean is5 = false;
	
	public TimeUpdate(Soviet s, MainLoader main, Balance mon, Upgrade up, Job job) {
		UPDATEFRM(s, main, mon, up, job);
	}
	
	private void UPDATEFRM(Soviet s, MainLoader main, Balance mon, Upgrade up, Job job) {
		UPD = new Timer(1, e -> {
			s.Balance.setText(main.TRANSLATE[8]+": "+new Coverting(mon.getMoney()).getStrMon()+" "+main.TRANSLATE[16]);
			s.COST.setText(main.TRANSLATE[11]+": "+new Coverting(up.getCost()).getStrMon()+" "+main.TRANSLATE[16]);
			s.LVL.setText(main.TRANSLATE[10]+": "+up.getLVL());
			
			s.YOURE_JOB.setText(main.TRANSLATE[18]+": "+main.TRANSLATE[19+job.getN()]+". "+main.TRANSLATE[17]+": "+
					job.getStandart()[job.getN()]+" "+main.TRANSLATE[16]);
			if(up.getLVL() == 5 && is5!=true) {
				s.UP.setEnabled(false); 
				s.AUTO.setVisible(true);
				is5=true;
			}
		});
		UPD.start();
	}
	
}
