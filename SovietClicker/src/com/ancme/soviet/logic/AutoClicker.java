package com.ancme.soviet.logic;

import javax.swing.Timer;

public class AutoClicker {

	private Timer clk;	
	private int i=0;
	
	public void StartClick(Job job, Upgrade up, Balance mon) {
		i++;
		if(i==1) {
			clk = new Timer(100, e -> {
				job.Sell(up, mon);
			});
			clk.start();
		}else if(i==2) {
			i-=2;
			clk.stop();
		}
	}
}
