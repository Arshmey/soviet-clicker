package com.ancme.soviet.logic;

public class Job {

	private int N = 0;
	private int[] Standart = { 215, 12746, 27531, 57645, 134978};
	
	public void Sell(Upgrade up, Balance mon) {
		mon.setMoney(mon.getMoney() + getStandart()[N]);
	}

	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int[] getStandart() {
		return Standart;
	}
	
}
