package com.ancme.soviet.logic;

import javax.swing.JOptionPane;

import com.ancme.soviet.loader.MainLoader;

public class Event {

	public Event(MainLoader main, Balance mon) {
		if(mon.getMoney() >= 50000000) {
			JOptionPane.showConfirmDialog(null, "Thank you for playing OUR game", main.TRANSLATE[15], 2, 1);
			System.exit(0);
		}
	}
	
}
