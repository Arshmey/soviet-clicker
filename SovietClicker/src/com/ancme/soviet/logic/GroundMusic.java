package com.ancme.soviet.logic;

import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class GroundMusic {

	Clip GM;
	File soundFile;
	public FloatControl volume;
	int v = 0;
	boolean isMuted = false;
	
	public GroundMusic() {
		GroundMusics();
	}
	
	public void GroundMusics() {
		try {
	        soundFile = new File("Sovet File\\USSR.wav"); 
	        AudioInputStream inAudio = AudioSystem.getAudioInputStream(soundFile);
	        GM = AudioSystem.getClip();
	        GM.open(inAudio);
	        GM.setFramePosition(0);
	        volume = (FloatControl) GM.getControl(FloatControl.Type.MASTER_GAIN);
	        
	        volume.setValue(-10);
	        GM.start();
		} catch (Exception ex) {} 
	}
	
	public void MuteOrUnmute() {
		v++;
		if(v == 1) {
			GM.stop();
			isMuted = true;
		}else if(v == 2) {
			isMuted = false;
			GM.start();
			v-=2;
		}
	}
	
	public void RESTART() {
		if(isMuted!=true) {
			GM.setFramePosition(0);
			GM.start();
		}
	}
	
}
