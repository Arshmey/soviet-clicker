package com.ancme.soviet.logic;

import java.io.IOException;

import com.ancme.soviet.loader.ChangeTranslate;
import com.ancme.soviet.loader.MainLoader;

public class Reset {

	public Reset(ChangeTranslate CT, MainLoader ML) {
		CT.Change(ML);
		try { 
			CT.SaveLoc(); 
			Runtime.getRuntime().exec("javaw -jar SovietClicker.jar"); 
		}
		catch (IOException e1) {}
		System.exit(0);
	}
	
}
