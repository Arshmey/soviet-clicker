package com.ancme.soviet.logic;

import javax.swing.ImageIcon;

import com.ancme.soviet.loader.MainLoader;
import com.ancme.soviet.win.Debug;
import com.ancme.soviet.win.Soviet;

public class DifficultyChanged {

	GroundMusic GM;
	
	public DifficultyChanged(MainLoader main, Upgrade up, Balance mon, int i, Job job, AutoClicker AC) {
		if(i == 2) {
			mon.setMoney(10000000);
			up.setLVL(5);
			job.setN(4);
		}
		if(i == 3) {
			mon.setMoney(10000);
			GM = new GroundMusic();
			new MusicUpd(GM, i);
		}
		if(i == 4) {
			GM = new GroundMusic();
			new MusicUpd(GM, i);
		}
		Soviet s = new Soviet(main, up, mon, i, GM, job, AC);
		if(i == 2) { s.REST.setEnabled(false); s.UP.setEnabled(false); }
		s.BACKGROUND.setIcon(new ImageIcon(main.IMG[2]));
		//new Debug(mon);
		new TimeUpdate(s, main, mon, up, job);
		
	}
			
}
