package com.ancme.soviet.logic;

public class Upgrade {

	private int LVL = 1;
	private int Cost = 10000;
	
	public int getLVL() {
		return LVL;
	}
	public void setLVL(int lVL) {
		LVL = lVL;
	}
	
	public int getCost() {
		return Cost;
	}
	public void setCost(int cost) {
		Cost = cost;
	}
	
	public void EventUpgrade(Balance mon, Job job) {
		if(mon.getMoney() >= Cost) {
			if(LVL != 5) {
				mon.setMoney(mon.getMoney()-Cost);
				Cost = Cost+26864;
				LVL += 1;
				job.setN(LVL-1);
			}
		}
	}
	
}
